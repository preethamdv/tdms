//
//  Customers.m
//  Tdms2.1
//
//  Created by Preeetham Dhondaley Venkatesh on 11/9/14.
//  Copyright (c) 2014 Preeetham Dhondaley Venkatesh. All rights reserved.
//

#import "Customers.h"


@implementation Customers

@dynamic age;
@dynamic balance;
@dynamic callDuration;
@dynamic campaign;
@dynamic contact;
@dynamic customerName;
@dynamic dayContacted;
@dynamic defaults;
@dynamic housing;
@dynamic loan;
@dynamic monthContacted;
@dynamic pdays;
@dynamic y;

@end