//
//  firstView.h
//  TDMS
//
//  Created by Preeetham Dhondaley Venkatesh on 11/1/14.
//  Copyright (c) 2014 Preeetham Dhondaley Venkatesh. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AppDelegate.h"

@interface firstView : NSViewController<NSTableViewDelegate,NSTableViewDataSource>
{

    
    __weak IBOutlet NSTableView *firstViewTable;
    id myAppDelegate;
    NSArray *customerRecords;
    BOOL inConsistenctButtonState;
    
    
  __weak IBOutlet NSMenuItem *cell;
    
}

@property (nonatomic) BOOL buttonEnable;

@property(readwrite)bool outcomeEditable;
@property(assign) BOOL inConsistenctButtonState;

- (IBAction)insertNewCustomer:(id)sender;
- (IBAction)reportProblem:(id)sender;


-(BOOL)processInformation:(NSString *)fullName age:(NSInteger)age defaults:(BOOL)defaults bankBalance:(NSNumber *)bankBalance housingLoan:(BOOL)housingLoan personalLoan:(BOOL)personalLoan contactMedium:(NSString *)contactMedium dayContacted:(NSInteger)dayContacted monthContacted:(NSString *)monthContacted callDuration:(NSNumber *)callDuration campaignsCount:(NSInteger)campaignsCount previousCount:(NSInteger)previousCount;

-(void)refreshTable;
-(void)insertTest;

-(void)initCustomerRecords;

@end