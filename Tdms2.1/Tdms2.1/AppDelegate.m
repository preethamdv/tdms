//
//  AppDelegate.m
//  Tdms2.1
//
//  Created by Preeetham Dhondaley Venkatesh on 11/9/14.
//  Copyright (c) 2014 Preeetham Dhondaley Venkatesh. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
- (IBAction)saveAction:(id)sender;

@end

@implementation AppDelegate
@synthesize ourViewController,ourView,arffTemplate;






-(id)init
{
    if ( (self = [super init]) ) {
        // your custom initialization
        sharedInitObject=self;
    }
    return self;
}





#pragma mark -

#pragma mark ***** My Init *****




-(void)awakeFromNib
{
    [self beginMyProgram];
    
    
}






-(void)beginMyProgram
{
    [self performSelectorInBackground:@selector(train) withObject:self];
    arffTemplate=[[NSString alloc]init];
    [self initView];
    //  [self initArff];
    
    
 //   [self insertCustomer:@"preetham" age:[NSNumber numberWithInt:24] defaults:NO bankBalance:[NSNumber numberWithInt:9999999] housingLoan:NO personalLoan:NO contactMedium:@"cellular" dayContacted:[NSNumber numberWithInt:2] monthContacted:@"jan" callDuration:[NSNumber numberWithInt:120] campaignsCount:[NSNumber numberWithInt:1] previousCount:[NSNumber numberWithInt:1]];
    
    
}








-(void)changeViewController:(NSInteger)tag
{
    NSLog(@"view will change");
    
    switch (tag)
    {
        case 0:
            [[ourViewController view]removeFromSuperview];
            
            //      self.ourViewController=[[LoginPage alloc] initWithNibName:@"LoginPage" bundle:nil];
            
            //       break;
        default:
            break;
    }
    
    [ourView addSubview:[ourViewController view]];
    [[ourViewController view] setFrame:[ourView bounds]];
    
    
    
}

static AppDelegate *sharedInitObject = nil;
+(AppDelegate *)sharedInit {
    if (sharedInitObject == nil) {
        sharedInitObject = [[super allocWithZone:NULL] init];
    }
    return sharedInitObject;   }











-(void)initView
{
    self.ourViewController=[[firstView alloc] initWithNibName:@"firstView" bundle:nil];
    [ourView addSubview:[ourViewController view]];
    
    
[[ourViewController view] setFrame:[ourView bounds]];
    
    
    
    
    [[ourViewController view] setTranslatesAutoresizingMaskIntoConstraints:NO];
    superview = [self.window contentView];
    
    myConstraint=[NSLayoutConstraint constraintWithItem:[ourViewController view] attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:superview attribute:NSLayoutAttributeWidth multiplier:1 constant:1];
    [superview addConstraint:myConstraint];
    
    
    myConstraint=[NSLayoutConstraint constraintWithItem:[ourViewController view] attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:superview attribute:NSLayoutAttributeHeight multiplier:1 constant:1];
    [superview addConstraint:myConstraint];
    
    
    
}






#pragma mark -

#pragma mark ***** Record Operations *****


//
//-(void)insertTest
//{
//
//    //contact:"unknown","telephone","cellular"
//
//
//
//    //        Customers *newCustomer=(Customers *)[NSEntityDescription insertNewObjectForEntityForName:@"Customers" inManagedObjectContext:_managedObjectContext];
//    //
//    //    [newCustomer setCustomerName:@"Preetham DV"];
//    //    [newCustomer setAge:[NSNumber numberWithInt:21]];
//    //    [newCustomer setBalance:[NSNumber numberWithInt:9999999]];
//    //    [newCustomer setCallDuration:[NSNumber numberWithInt:20]];
//    //    [newCustomer setCampaign:[NSNumber numberWithInt:1]];
//    //    [newCustomer setContact:@"cellular"];
//    //    [newCustomer setDayContacted:[NSNumber numberWithInt:2]];
//    //    [newCustomer setDefaults:NO];
//    //    [newCustomer setHousing:NO];
//    //    [newCustomer setY:YES];
//    //    [newCustomer setPdays:[NSNumber numberWithInt:32]];
//    //    [newCustomer setMonthContacted:[NSNumber numberWithInt:5]];
//    //    [newCustomer setLoan:NO];
//    //
//    //
//
//
//
//    for(int i=0;i<10;i++)
//    {
//
//        NSManagedObject *object = [NSEntityDescription insertNewObjectForEntityForName:@"Customers"
//                                                                inManagedObjectContext:self.managedObjectContext];
//
//
//
//
//        [object setValue:[NSString stringWithFormat:@"preetham%d",i] forKey:@"customerName"];
//        [object setValue:[NSNumber numberWithInt:21] forKey:@"age"];
//        [object setValue:[NSNumber numberWithInt:9999999] forKey:@"balance"];
//        [object setValue:[NSNumber numberWithInt:20] forKey:@"callDuration"];
//        [object setValue:[NSNumber numberWithInt:1] forKey:@"campaign"];
//        [object setValue:@"cellular" forKey:@"contact"];
//        [object setValue:[NSNumber numberWithInt:2] forKey:@"dayContacted"];
//        [object setValue:@NO forKey:@"defaults"];
//        [object setValue:@NO forKey:@"housing"];
//        [object setValue:@NO forKey:@"loan"];
//        [object setValue:[NSNumber numberWithInt:32] forKey:@"pdays"];
//        [object setValue:@"jan" forKey:@"monthContacted"];
//        [object setValue:@YES forKey:@"y"];
//
//
//        NSError *error;
//        if (![self.managedObjectContext save:&error]) {
//            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
//        }
//    }
//
//}
//

-(void)deleteRecords:(NSNumber *)age balance:(NSNumber *)balance callDuration:(NSNumber *)callDuration campaign:(NSNumber *)campaign contact:(NSString *)contact customerName:(NSString *)customerName dayContacted:(NSNumber *)dayContacted housing:(NSNumber *)housing loan:(NSNumber *)loan monthContacted:(NSString *)monthContacted pdays:(NSNumber *)pdays y:(NSNumber *)y
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Customers"];
    NSPredicate *constraint=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"customerName like '%@' && age=%@ && balance=%@ && callDuration=%@ && campaign=%@ && contact like '%@' && dayContacted=%@ && housing=%@ && loan=%@ && monthContacted  like '%@' && pdays=%@ && y=%@",customerName,age,balance,callDuration,campaign,contact,dayContacted,housing,loan,monthContacted,pdays,y]];
    
  //  NSPredicate *constraint=[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"contact like 'unknown'"]];
    
    
    [request setPredicate:constraint];
    
    NSError *error;
    NSArray *fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
    NSLog(@"%lu records found",(unsigned long)[fetchResults count]);
    
    
    
    for(id delObj in fetchResults)
    {
        
        [self.managedObjectContext deleteObject:delObj];
        NSNotificationCenter *nc=[NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"refreshCustomerTable" object:self userInfo:nil];
    }
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
/*
 
 @property (nonatomic, retain) NSNumber * age;
 @property (nonatomic, retain) NSNumber * balance;
 @property (nonatomic, retain) NSNumber * callDuration;
 @property (nonatomic, retain) NSNumber * campaign;
 @property (nonatomic, retain) NSString * contact;
 @property (nonatomic, retain) NSString * customerName;
 @property (nonatomic, retain) NSNumber * dayContacted;
 @property (nonatomic, retain) NSNumber * defaults;
 @property (nonatomic, retain) NSNumber * housing;
 @property (nonatomic, retain) NSNumber * loan;
 @property (nonatomic, retain) NSString * monthContacted;
 @property (nonatomic, retain) NSNumber * pdays;
 @property (nonatomic, retain) NSNumber * y;
 
 
 
 */


-(NSArray *)fetchAllRecords
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Customers"];
    //  [request setFetchBatchSize:200];
    
    
    NSError *error;
    NSArray *fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    
    
    
    NSLog(@"%lu records found",(unsigned long)[fetchResults count]);
  //  NSLog(@"first rec name %@",[[fetchResults objectAtIndex:0]valueForKey:@"balance"]);
    
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"customerName" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    NSArray *sortedArray = [fetchResults sortedArrayUsingDescriptors:sortDescriptors];
    
    fetchResults =[NSArray arrayWithArray:sortedArray];
    
    
    return fetchResults;
    
    //  [self deleteTest];
    
}





-(void)deleteTest
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Customers"];
    NSPredicate *constraint=[NSPredicate predicateWithFormat:@"customerName like 'preetham4' && age=21"];
    [request setPredicate:constraint];
    
    NSError *error;
    NSArray *fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
    NSLog(@"%lu records found",(unsigned long)[fetchResults count]);
    
    
    
    for(id delObj in fetchResults)
    {
        
        [self.managedObjectContext deleteObject:delObj];
    }
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
}


-(IBAction)deleteRecords:(id)sender
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Customers"];
    
    NSError *error;
    NSArray *fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
    NSLog(@"%lu records found",(unsigned long)[fetchResults count]);
    
    
    
    for(id delObj in fetchResults)
    {
        
        [self.managedObjectContext deleteObject:delObj];
    }
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    NSNotificationCenter *nc=[NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"refreshCustomerTable" object:self userInfo:nil];
    
    
}


-(void)insertCustomer:(NSString *)fullName age:(NSNumber *)age defaults:(BOOL)defaults bankBalance:(NSNumber *)bankBalance housingLoan:(BOOL)housingLoan personalLoan:(BOOL)personalLoan contactMedium:(NSString *)contactMedium dayContacted:(NSNumber *)dayContacted monthContacted:(NSString *)monthContacted callDuration:(NSNumber *)callDuration campaignsCount:(NSNumber *)campaignsCount previousCount:(NSNumber *)previousCount
{
    
    
    //    NSString *loan=[[NSString alloc]init];
    //    NSString *housing=[[NSString alloc]init];
    //    NSString *currentDefaults=[[NSString alloc]init];
    //
    //
    //    if(defaults==YES)
    //    {
    //        currentDefaults=@"YES";
    //    }
    //    else{
    //        currentDefaults=@"NO";
    //    }
    //
    //    if(personalLoan==YES)
    //    {
    //        loan=@"YES";
    //    }
    //    else{
    //        loan=@"NO";
    //    }
    //    if(housingLoan==YES)
    //    {
    //        housing=@"YES";
    //    }
    //    else{
    //        housing=@"no";
    //    }
    
    NSManagedObject *object = [NSEntityDescription insertNewObjectForEntityForName:@"Customers"
                                                            inManagedObjectContext:self.managedObjectContext];
    
    
    
    
    [object setValue:fullName forKey:@"customerName"];
    [object setValue:age forKey:@"age"];
    [object setValue:bankBalance forKey:@"balance"];
    [object setValue:callDuration forKey:@"callDuration"];
    [object setValue:campaignsCount forKey:@"campaign"];
    [object setValue:contactMedium forKey:@"contact"];
    [object setValue:dayContacted forKey:@"dayContacted"];
    
    if(defaults)
        [object setValue:@YES forKey:@"defaults"];
    else
        [object setValue:@NO forKey:@"defaults"];
    
    if(housingLoan)
        [object setValue:@YES forKey:@"housing"];
    else
        [object setValue:@NO forKey:@"housing"];
    
    if(personalLoan)
        [object setValue:@YES forKey:@"loan"];
    else
        [object setValue:@NO forKey:@"loan"];
    
    [object setValue:previousCount forKey:@"pdays"];
    [object setValue:monthContacted forKey:@"monthContacted"];
    
    if([self testArff:age defaults:defaults bankBalance:bankBalance housingLoan:housingLoan personalLoan:personalLoan contactMedium:contactMedium dayContacted:dayContacted monthContacted:monthContacted callDuration:callDuration campaignsCount:campaignsCount previousCount:previousCount])
    {
        [object setValue:@YES forKey:@"y"];
    }
    else
    {
        [object setValue:@NO forKey:@"y"];
    }
    
    
    
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    NSNotificationCenter *nc=[NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"refreshCustomerTable" object:self userInfo:nil];
    
}


#pragma mark -

#pragma mark ***** Dataset  Operations*****



-(BOOL)testArff:(NSNumber *)age defaults:(BOOL)defaults bankBalance:(NSNumber *)bankBalance housingLoan:(BOOL)housingLoan personalLoan:(BOOL)personalLoan contactMedium:(NSString *)contactMedium dayContacted:(NSNumber *)dayContacted monthContacted:(NSString *)monthContacted callDuration:(NSNumber *)callDuration campaignsCount:(NSNumber *)campaignsCount previousCount:(NSNumber *)previousCount
{
    
    NSString *grepOutput;
    NSString *loan=[[NSString alloc]init];
    NSString *housing=[[NSString alloc]init];
    NSString *currentDefaults=[[NSString alloc]init];
    
    if(defaults==YES)
    {
        currentDefaults=@"yes";
    }
    else{
        currentDefaults=@"no";
    }
    
    if(personalLoan==YES)
    {
        loan=@"yes";
    }
    else{
        loan=@"no";
    }
    if(housingLoan==YES)
    {
        housing=@"yes";
    }
    else{
        housing=@"no";
    }
    
    
    
    
    
    arffTemplate=[NSString stringWithFormat:@"@RELATION bank-full-new.arff\n@ATTRIBUTE age REAL\n@ATTRIBUTE default {no,yes}\n@ATTRIBUTE balance REAL\n@ATTRIBUTE housing {yes,no}\n@ATTRIBUTE loan {no,yes}\n@ATTRIBUTE contact {unknown,cellular,telephone}\n@ATTRIBUTE day REAL\n@ATTRIBUTE month {may,jun,jul,aug,oct,nov,dec,jan,feb,mar,apr,sep}\n@ATTRIBUTE duration REAL\n@ATTRIBUTE campaign REAL\n@ATTRIBUTE previous REAL\n@ATTRIBUTE y {no,yes}\n@DATA\n%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,?",age,currentDefaults,bankBalance,housing,loan,contactMedium,dayContacted,monthContacted,callDuration,campaignsCount,previousCount];
    
    
    
    
 //   NSLog(@"%@",arffTemplate);
    
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle bundlePath];
    NSString *parentDirectory=path;
    path=[path stringByAppendingString:[NSString stringWithFormat:@"/Contents/Resources/weka-3-6-11-oracle-jvm.app/Contents/Java/weka.jar"]];
    NSString *testPath=[NSString stringWithFormat:@"%@/Contents/Resources/testFile.arff",parentDirectory];
    
    NSError *error;
    
    [[NSFileManager defaultManager]removeItemAtPath:testPath error:&error];
    
    [[NSFileManager defaultManager]createFileAtPath:testPath contents:[arffTemplate dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    
    BOOL fileExist=  [[NSFileManager defaultManager]fileExistsAtPath:[NSString stringWithFormat:@"%@/Contents/Resources/TDMS.model",parentDirectory]];
    
    if(fileExist)
    {
        
        
              
        //      int pid = [[NSProcessInfo processInfo] processIdentifier];
        NSPipe *pipe = [NSPipe pipe];
        NSFileHandle *file = pipe.fileHandleForReading;
        NSTask *task = [[NSTask alloc] init];
        [task setLaunchPath:@"/bin/sh"];
        
        [task setCurrentDirectoryPath:parentDirectory];
        
//        NSString *argumentString=[NSString stringWithFormat:@"export CLASSPATH=$CLASSPATH:%@ ;java weka.classifiers.trees.J48 -l %@/Contents/Resources/TDMS.model -T %@ -p 0 | cut -c26-28 | sed -n '6p'| sed  's/:no/no/g'",path,parentDirectory,testPath];
        
        NSString *argumentString=[NSString stringWithFormat:@"export CLASSPATH=$CLASSPATH:%@ ;java weka.classifiers.functions.MultilayerPerceptron -l %@/Contents/Resources/TDMS.model -T %@ -p 0 | cut -c26-28 | sed -n '6p'| sed  's/:no/no/g'",path,parentDirectory,testPath];
        
        
        [task setArguments:[NSArray arrayWithObjects:@"-c",argumentString,nil]];
        task.standardOutput = pipe;
        
        [task launch];
        //   [task waitUntilExit];
        
        NSData *data = [file readDataToEndOfFile];
        [file closeFile];
        
        grepOutput = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog (@"Terminal returned:..%@..", grepOutput);
        
        NSNotificationCenter *nc2=[NSNotificationCenter defaultCenter];
        [nc2 postNotificationName:@"trainingApplication" object:self userInfo:[NSDictionary dictionaryWithObject:@"no" forKey:@"trainingStat"]];
    }
    
    
    if([grepOutput containsString:@"no"])
    {
        return NO;
    }
    else
    {
        NSLog(@"Will return yes");
        return YES;
    }
    
}



-(void)train
{
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle bundlePath];
    NSString *parentDirectory=path;
    path=[path stringByAppendingString:[NSString stringWithFormat:@"/Contents/Resources/weka-3-6-11-oracle-jvm.app/Contents/Java/weka.jar"]];
    
    BOOL fileExist=  [[NSFileManager defaultManager]fileExistsAtPath:[NSString stringWithFormat:@"%@/Contents/Resources/TDMS.model",parentDirectory]];
    
    if(!fileExist)
    {
        
        //      int pid = [[NSProcessInfo processInfo] processIdentifier];
        NSPipe *pipe = [NSPipe pipe];
        NSFileHandle *file = pipe.fileHandleForReading;
        NSTask *task = [[NSTask alloc] init];
        [task setLaunchPath:@"/bin/sh"];
        
        [task setCurrentDirectoryPath:parentDirectory];
        
    //    NSString *argumentString=[NSString stringWithFormat:@"export CLASSPATH=$CLASSPATH:%@ ; java weka.classifiers.trees.J48 -t %@/Contents/Resources/filteredTrainingSet.arff -d %@/Contents/Resources/TDMS.model -no-cv",path,parentDirectory,parentDirectory];
        
        
        NSString *argumentString=[NSString stringWithFormat:@"export CLASSPATH=$CLASSPATH:%@ ; java weka.classifiers.functions.MultilayerPerceptron -t %@/Contents/Resources/filteredTrainingSet.arff -d %@/Contents/Resources/TDMS.model -no-cv",path,parentDirectory,parentDirectory];
        
        [task setArguments:[NSArray arrayWithObjects:@"-c",argumentString,nil]];
        task.standardOutput = pipe;
        
        [task launch];
        //   [task waitUntilExit];
        
        NSData *data = [file readDataToEndOfFile];
        [file closeFile];
        
        NSString *grepOutput = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog (@"grep returned:\n%@", grepOutput);
    }
    else
    {
        NSLog(@"Dataset Already trained");
    }
}





























#pragma mark -
#pragma mark ***** Built In Methods *****







- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

#pragma mark - Core Data stack

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedObjectContext = _managedObjectContext;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "Preetham.Tdms2_1" in the user's Application Support directory.
    NSURL *appSupportURL = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:@"Preetham.Tdms2_1"];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
	
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Tdms2_1" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.)
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *applicationDocumentsDirectory = [self applicationDocumentsDirectory];
    BOOL shouldFail = NO;
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    // Make sure the application files directory is there
    NSDictionary *properties = [applicationDocumentsDirectory resourceValuesForKeys:@[NSURLIsDirectoryKey] error:&error];
    if (properties) {
        if (![properties[NSURLIsDirectoryKey] boolValue]) {
            failureReason = [NSString stringWithFormat:@"Expected a folder to store application data, found a file (%@).", [applicationDocumentsDirectory path]];
            shouldFail = YES;
        }
    } else if ([error code] == NSFileReadNoSuchFileError) {
        error = nil;
        [fileManager createDirectoryAtPath:[applicationDocumentsDirectory path] withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    if (!shouldFail && !error) {
        NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        NSURL *url = [applicationDocumentsDirectory URLByAppendingPathComponent:@"OSXCoreDataObjC.storedata"];
        if (![coordinator addPersistentStoreWithType:NSXMLStoreType configuration:nil URL:url options:nil error:&error]) {
            coordinator = nil;
        }
        _persistentStoreCoordinator = coordinator;
    }
    
    if (shouldFail || error) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        if (error) {
            dict[NSUnderlyingErrorKey] = error;
        }
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        [[NSApplication sharedApplication] presentError:error];
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];

    return _managedObjectContext;
}

#pragma mark - Core Data Saving and Undo support

- (IBAction)saveAction:(id)sender {
    // Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
    }
    
    NSError *error = nil;
    if ([[self managedObjectContext] hasChanges] && ![[self managedObjectContext] save:&error]) {
        [[NSApplication sharedApplication] presentError:error];
    }
}

- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window {
    // Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
    return [[self managedObjectContext] undoManager];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    // Save changes in the application's managed object context before the application terminates.
    
    if (!_managedObjectContext) {
        return NSTerminateNow;
    }
    
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing to terminate", [self class], NSStringFromSelector(_cmd));
        return NSTerminateCancel;
    }
    
    if (![[self managedObjectContext] hasChanges]) {
        return NSTerminateNow;
    }
    
    NSError *error = nil;
    if (![[self managedObjectContext] save:&error]) {

        // Customize this code block to include application-specific recovery steps.              
        BOOL result = [sender presentError:error];
        if (result) {
            return NSTerminateCancel;
        }

        NSString *question = NSLocalizedString(@"Could not save changes while quitting. Quit anyway?", @"Quit without saves error question message");
        NSString *info = NSLocalizedString(@"Quitting now will lose any changes you have made since the last successful save", @"Quit without saves error question info");
        NSString *quitButton = NSLocalizedString(@"Quit anyway", @"Quit anyway button title");
        NSString *cancelButton = NSLocalizedString(@"Cancel", @"Cancel button title");
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:question];
        [alert setInformativeText:info];
        [alert addButtonWithTitle:quitButton];
        [alert addButtonWithTitle:cancelButton];

        NSInteger answer = [alert runModal];
        
        if (answer == NSAlertFirstButtonReturn) {
            return NSTerminateCancel;
        }
    }

    return NSTerminateNow;
}

@end
