//
//  AppDelegate.h
//  Tdms2.1
//
//  Created by Preeetham Dhondaley Venkatesh on 11/9/14.
//  Copyright (c) 2014 Preeetham Dhondaley Venkatesh. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "firstView.h"
#import "Customers.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>
{
    IBOutlet NSView *ourView;
    NSViewController *ourViewController;
    NSView *superview;
    NSLayoutConstraint *myConstraint;
    NSString *arffTemplate;
    
    
    
}
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (readonly, strong, nonatomic) NSString *arffTemplate;



@property (readwrite,strong,nonatomic) NSViewController *ourViewController;
@property (readwrite,strong,nonatomic) IBOutlet NSView *ourView;

-(void)changeViewController:(NSInteger)tag;
+(AppDelegate *)sharedInit;


//-(void)insertTest;
-(NSArray *)fetchAllRecords;
-(void)deleteTest;
//-(void)resetData;
-(void)train;

-(void)insertCustomer:(NSString *)fullName age:(NSNumber *)age defaults:(BOOL)defaults bankBalance:(NSNumber *)bankBalance housingLoan:(BOOL)housingLoan personalLoan:(BOOL)personalLoan contactMedium:(NSString *)contactMedium dayContacted:(NSNumber *)dayContacted monthContacted:(NSString *)monthContacted callDuration:(NSNumber *)callDuration campaignsCount:(NSNumber *)campaignsCount previousCount:(NSNumber *)previousCount;



-(BOOL)testArff:(NSNumber *)age defaults:(BOOL)defaults bankBalance:(NSNumber *)bankBalance housingLoan:(BOOL)housingLoan personalLoan:(BOOL)personalLoan contactMedium:(NSString *)contactMedium dayContacted:(NSNumber *)dayContacted monthContacted:(NSString *)monthContacted callDuration:(NSNumber *)callDuration campaignsCount:(NSNumber *)campaignsCount previousCount:(NSNumber *)previousCount;

-(void)deleteRecords:(NSNumber *)age balance:(NSNumber *)balance callDuration:(NSNumber *)callDuration campaign:(NSNumber *)campaign contact:(NSString *)contact customerName:(NSString *)customerName dayContacted:(NSNumber *)dayContacted  housing:(NSNumber *)housing loan:(NSNumber *)loan monthContacted:(NSString *)monthContacted pdays:(NSNumber *)pdays y:(NSNumber *)y;




-(IBAction)deleteRecords:(id)sender;

@end

