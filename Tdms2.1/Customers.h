//
//  Customers.h
//  Tdms2.1
//
//  Created by Preeetham Dhondaley Venkatesh on 11/9/14.
//  Copyright (c) 2014 Preeetham Dhondaley Venkatesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Customers : NSManagedObject

@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSNumber * balance;
@property (nonatomic, retain) NSNumber * callDuration;
@property (nonatomic, retain) NSNumber * campaign;
@property (nonatomic, retain) NSString * contact;
@property (nonatomic, retain) NSString * customerName;
@property (nonatomic, retain) NSNumber * dayContacted;
@property (nonatomic, retain) NSNumber * defaults;
@property (nonatomic, retain) NSNumber * housing;
@property (nonatomic, retain) NSNumber * loan;
@property (nonatomic, retain) NSString * monthContacted;
@property (nonatomic, retain) NSNumber * pdays;
@property (nonatomic, retain) NSNumber * y;

@end
