//
//  newCustomerWindow.h
//  Tdms2.1
//
//  Created by Preeetham Dhondaley Venkatesh on 11/9/14.
//  Copyright (c) 2014 Preeetham Dhondaley Venkatesh. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AppDelegate.h"

@interface newCustomerWindow : NSWindowController
{
    id myAppDelegate;

    __weak IBOutlet NSPopUpButton *monthContactedBox;
    __weak IBOutlet NSPopUpButton *dayContactedBox;
    __weak IBOutlet NSTextField *callDurationBox;
    __weak IBOutlet NSTextField *previousCampaignsBox;
    __weak IBOutlet NSTextField *campaignsBox;
    __weak IBOutlet NSPopUpButton *contactTypeBox;
    __weak IBOutlet NSPopUpButton *personalLoanBox;
    __weak IBOutlet NSPopUpButton *defaultsLoanBox;
    __weak IBOutlet NSPopUpButton *housingLoanBox;
    __weak IBOutlet NSTextField *accBalanceBox;
    __weak IBOutlet NSTextField *ageBox;
    __weak IBOutlet NSTextField *nameBox;
}

- (IBAction)createNewCustomer:(id)sender;
@end
