//
//  newCustomerWindow.m
//  Tdms2.1
//
//  Created by Preeetham Dhondaley Venkatesh on 11/9/14.
//  Copyright (c) 2014 Preeetham Dhondaley Venkatesh. All rights reserved.
//

#import "newCustomerWindow.h"

@interface newCustomerWindow ()

@end

@implementation newCustomerWindow

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    
    
    myAppDelegate=[AppDelegate sharedInit];

    
    
    
}

-(id)init
{

    self=[super init];
    [self.window setTitle:@"New Customer"];
    
    if(![super initWithWindowNibName:@"newCustomerWindow"])
    {
        return nil;
    }
    return self;
}

- (IBAction)createNewCustomer:(id)sender {
    
    
    BOOL defaults;
    BOOL housingLoan;
    BOOL personalLoan;
    
    if([[[defaultsLoanBox selectedItem]title]isEqualToString:@"YES"])
    {
        defaults=YES;
    }
    else{
        defaults=NO;
    }
    if([[[housingLoanBox selectedItem]title]isEqualToString:@"YES"])
    {
        housingLoan=YES;
    }
    else
    {
        housingLoan=NO;
    }
    if([[[personalLoanBox selectedItem]title]isEqualToString:@"YES"])
    {
        personalLoan=YES;
    }
    else{
        personalLoan=NO;
    }
    
    [myAppDelegate insertCustomer:[nameBox stringValue] age:[NSNumber numberWithInteger:[ageBox integerValue]] defaults:defaults bankBalance:[NSNumber numberWithInteger:[accBalanceBox integerValue]]housingLoan:housingLoan personalLoan:personalLoan contactMedium:[[contactTypeBox selectedItem]title] dayContacted:[NSNumber numberWithInteger:[[[dayContactedBox selectedItem]title]integerValue] ]monthContacted:[[monthContactedBox selectedItem]title] callDuration:[NSNumber numberWithInteger:[callDurationBox integerValue]] campaignsCount:[NSNumber numberWithInteger:[campaignsBox integerValue]] previousCount:[NSNumber numberWithInteger:[previousCampaignsBox integerValue] ]];
    
    
    

}
@end
