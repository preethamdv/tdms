//
//  firstView.m
//  TDMS
//
//  Created by Preeetham Dhondaley Venkatesh on 11/1/14.
//  Copyright (c) 2014 Preeetham Dhondaley Venkatesh. All rights reserved.
//

#import "firstView.h"

@interface firstView ()
@end

@implementation firstView

@synthesize buttonEnable,inConsistenctButtonState;




#pragma mark -
#pragma mark ***** My Init *****


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [self setButtonEnable:YES];
    myAppDelegate=[AppDelegate sharedInit];
    _outcomeEditable=NO;
    customerRecords=[[NSArray alloc]init];
    [self initCustomerRecords];
    [firstViewTable setAllowsMultipleSelection:TRUE];
    
    NSNotificationCenter *notfn=[NSNotificationCenter defaultCenter];
    [notfn addObserver:self selector:@selector(refreshTable) name:@"refreshCustomerTable" object:nil];
      
    
  //   [myAppDelegate resetData];

    
 //   [self insertTest];
  //  [self testArff];
 //   [myAppDelegate fetchTest];
   

}





-(void)keyDown:(NSEvent *)theEvent
{

    
    if([theEvent keyCode]==51)
    {
        NSIndexSet *selectedIndexes=[firstViewTable selectedRowIndexes];
       if([selectedIndexes count]>0)
       {
           [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx,BOOL *stop)
            {
                NSLog(@"%lu",(unsigned long)idx);
                NSLog(@"%@",[[customerRecords objectAtIndex:idx] customerName]);
                [myAppDelegate deleteRecords:[[customerRecords objectAtIndex:idx] age] balance:[[customerRecords objectAtIndex:idx] balance] callDuration:[[customerRecords objectAtIndex:idx] callDuration] campaign:[[customerRecords objectAtIndex:idx] campaign] contact:[[customerRecords objectAtIndex:idx] contact] customerName:[[customerRecords objectAtIndex:idx] customerName] dayContacted:[[customerRecords objectAtIndex:idx] dayContacted] housing:[[customerRecords objectAtIndex:idx] housing] loan:[[customerRecords objectAtIndex:idx] loan] monthContacted:[[customerRecords objectAtIndex:idx] monthContacted] pdays:[[customerRecords objectAtIndex:idx] pdays] y:[[customerRecords objectAtIndex:idx] y]];
            }];
           
        //   NSLog(@"%lu",(unsigned long)[selectedIndexes firstIndex]);
       }
        else
        {
            NSBeep();
        }
    }
}


-(void)initCustomerRecords
{
    customerRecords=[[NSArray alloc] initWithArray:[myAppDelegate fetchAllRecords]];
}


-(void)refreshTable
{
    [self initCustomerRecords];
    [firstViewTable reloadData];
}

#pragma mark -
#pragma mark ***** TableView Delegates *****


-(NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView
{
    return [customerRecords count];
}



- (void)tableView:(NSTableView *)tableView willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
   // NSLog(@"asdasdasdsdsad");
    
    

}
- (void)tableView:(NSTableView *)aTableView didClickTableColumn:(NSTableColumn *)aTableColumn
{
    [aTableView setIndicatorImage:[NSImage imageNamed:@"NSAscendingSortIndicator"] inTableColumn:aTableColumn];
    [aTableView reloadData];
}


-(id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    

    NSString *identifier=[tableColumn identifier];


    if([identifier isEqualToString:@"nameIdentifier"]){
        return [[customerRecords objectAtIndex:row] valueForKey:@"customerName"];}
    else if([identifier isEqualToString:@"ageIdentifier"])
        return [[customerRecords objectAtIndex:row] valueForKey:@"age"];
    else if([identifier isEqualToString:@"defaultsIdentifier"])
    {
        if([[[customerRecords objectAtIndex:row] valueForKey:@"defaults"] integerValue] ==1)
            return @"Yes";
            else
            return @"No";
    }
    else if([identifier isEqualToString:@"balanceIdentifier"])
    {  return [[customerRecords objectAtIndex:row] valueForKey:@"balance"];}
    else if([identifier isEqualToString:@"housingLoanIdentifier"])
    {
        if([[[customerRecords objectAtIndex:row] valueForKey:@"housing"] integerValue]==1)
        return @"Yes";
        else
            return @"No";
    }
    else if([identifier isEqualToString:@"personalLoanIdentifier"])
    {
        if([[[customerRecords objectAtIndex:row] valueForKey:@"loan"] integerValue]==1)
        return @"Yes";
        else
            return @"No";
    }
    else if([identifier isEqualToString:@"contactTypeIdentifier"])
    {  return [[customerRecords objectAtIndex:row] valueForKey:@"contact"];}
    else if([identifier isEqualToString:@"dayIdentifier"])
    {return [[customerRecords objectAtIndex:row] valueForKey:@"dayContacted"];}
    else if([identifier isEqualToString:@"monthIdentifier"])
    {  return [[customerRecords objectAtIndex:row] valueForKey:@"monthContacted"];}
    else if([identifier isEqualToString:@"callDurationdentifier"])
    {  return [[customerRecords objectAtIndex:row] valueForKey:@"callDuration"];}
    else if([identifier isEqualToString:@"campaignsIdentifier"])
    {  return [[customerRecords objectAtIndex:row] valueForKey:@"campaign"];}
    else if([identifier isEqualToString:@"previousCampaignsIdentifier"])
    { return [[customerRecords objectAtIndex:row] valueForKey:@"pdays"];}
    else {
        if([[[customerRecords objectAtIndex:row] valueForKey:@"y"] integerValue]==1)
        return @"Yes";
        else
            return @"No";
    }
    
}

- (void)tableView:(NSTableView *)tableView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
///    NSLog(@"object %@",object);
}



#pragma -
#pragma mark ***** Information Processing *****


- (IBAction)reportProblem:(id)sender
{
    NSLog(@"Inconsistency Detected");
}



- (IBAction)insertNewCustomer:(id)sender {
    
    
    
}

-(BOOL)processInformation:(NSString *)fullName age:(NSInteger)age defaults:(BOOL)defaults bankBalance:(NSNumber *)bankBalance housingLoan:(BOOL)housingLoan personalLoan:(BOOL)personalLoan contactMedium:(NSString *)contactMedium dayContacted:(NSInteger)dayContacted monthContacted:(NSString *)monthContacted callDuration:(NSNumber *)callDuration campaignsCount:(NSInteger)campaignsCount previousCount:(NSInteger)previousCount
{
    
    return TRUE;
}





/*
-(void)testArff
{
    NSString *arffTemplate;
    
    arffTemplate=@"@RELATION bank-full-new.arff\n@ATTRIBUTE age REAL\n@ATTRIBUTE default {no,yes}\n@ATTRIBUTE balance REAL\n@ATTRIBUTE housing {yes,no}\n@ATTRIBUTE loan {no,yes}\n@ATTRIBUTE contact {unknown,cellular,telephone}\n@ATTRIBUTE day REAL\n@ATTRIBUTE month {may,jun,jul,aug,oct,nov,dec,jan,feb,mar,apr,sep}\n@ATTRIBUTE duration REAL\n@ATTRIBUTE campaign REAL\n@ATTRIBUTE previous REAL\n@ATTRIBUTE y {no,yes}\n@DATA\n58,no,2143,yes,no,unknown,5,may,261,1,0,?";
    
    
    
    
    
    
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle bundlePath];
    NSString *parentDirectory=path;
    path=[path stringByAppendingString:[NSString stringWithFormat:@"/Contents/Resources/weka-3-6-11-oracle-jvm.app/Contents/Java/weka.jar"]];
    NSString *testPath=[NSString stringWithFormat:@"%@/Contents/Resources/testFile.arff",parentDirectory];
    
    NSError *error;
    
    [[NSFileManager defaultManager]removeItemAtPath:testPath error:&error];
    
    [[NSFileManager defaultManager]createFileAtPath:testPath contents:[arffTemplate dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    
    BOOL fileExist=  [[NSFileManager defaultManager]fileExistsAtPath:[NSString stringWithFormat:@"%@/Contents/Resources/TDMS.model",parentDirectory]];
    
    if(fileExist)
    {
        
        //      int pid = [[NSProcessInfo processInfo] processIdentifier];
        NSPipe *pipe = [NSPipe pipe];
        NSFileHandle *file = pipe.fileHandleForReading;
        NSTask *task = [[NSTask alloc] init];
        [task setLaunchPath:@"/bin/sh"];
        
        [task setCurrentDirectoryPath:parentDirectory];
        
        NSString *argumentString=[NSString stringWithFormat:@"export CLASSPATH=$CLASSPATH:%@ ;java weka.classifiers.trees.J48 -l %@/Contents/Resources/TDMS.model -T %@ -p 0 | cut -c26-28 | sed -n '6,${p;}' | sed  's/:no/no/g'",path,parentDirectory,testPath];
        
        [task setArguments:[NSArray arrayWithObjects:@"-c",argumentString,nil]];
        task.standardOutput = pipe;
        
        [task launch];
        //   [task waitUntilExit];
        
        NSData *data = [file readDataToEndOfFile];
        [file closeFile];
        
        NSString *grepOutput = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog (@"Terminal returned:\n%@", grepOutput);
    }

    
    
    
    
    
    
    
    
    
    
}


*/

-(void)insertTest
{
    [myAppDelegate insertTest];
}





@end

















//NSManagedObjectContext
/*
 NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"CustomersDB"];
 [request setFetchBatchSize:20];
 
 NSError *error;
 NSArray *fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
 
 
 NSLog(@"%lu records found",(unsigned long)[fetchResults count]);
 
 if(fetchResults==nil)
 {
 //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
 //  abort();
 
 
 //    Customers *newCustomer=(Customers *)[NSEntityDescription //insertNewObjectForEntityForName:@"CustomerDB" inManagedObjectContext:_managedObjectContext];
 //    [newCustomer setCustomerName:@"Preetham DV"];
 */
/*
 NSManagedObject *object = [NSEntityDescription insertNewObjectForEntityForName:@"CustomerDB"
 inManagedObjectContext:self.managedObjectContext];
 [object setValue:@"preetham" forKey:@"customerName"];
 NSError *error;
 if (![self.managedObjectContext save:&error]) {
 NSLog(@"Failed to save - error: %@", [error localizedDescription]);
 }
 *//*}
    else
    {
    NSLog(@"%lu records found",(unsigned long)[fetchResults count]);
    
    }
    */



/*
 
 NSView *superview = [self view];
 NSLayoutConstraint *myConstraint;
 
 
 NSBox *topBox = [[NSBox alloc] initWithFrame:[[[self view]superview]bounds]];
 [topBox setBoxType:NSBoxPrimary];
 [topBox setTranslatesAutoresizingMaskIntoConstraints:NO];
 [topBox setBorderType:NSBezelBorder];
 [topBox setTitle:@""];
 [superview addSubview:topBox];
 
 myConstraint=[NSLayoutConstraint constraintWithItem:[self view] attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:topBox attribute:NSLayoutAttributeWidth multiplier:1 constant:50];
 [superview addConstraint:myConstraint];
 
 myConstraint=[NSLayoutConstraint constraintWithItem:topBox attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:50];
 [superview addConstraint:myConstraint];
 
 */

